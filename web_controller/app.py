#!/usr/bin/env python3
import os
from time import time
import json
import redis
import logging
from flask import Flask, jsonify, request, Response
from flask_cors import CORS
from minio import Minio
from minio.error import (BucketAlreadyOwnedByYou, BucketAlreadyExists, ResponseError)
import hashlib
import io
from werkzeug.exceptions import InternalServerError
from http import HTTPStatus
from flask_socketio import SocketIO
from pymongo import MongoClient


app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = '/uploaded'
CORS(app)
# cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
socketio = SocketIO(app)

MINIO_URL = os.getenv('MINIO_URL', 'localhost')
MINIO_ACCESS_KEY = os.getenv('MINIO_ACCESS_KEY')
MINIO_SECRET_KEY = os.getenv('MINIO_SECRET_KEY')

minioClient = Minio(f'{MINIO_URL}:9000',
                    access_key=MINIO_ACCESS_KEY,
                    secret_key=MINIO_SECRET_KEY,
                    secure=False)


MONGO = os.getenv('MONGO', 'localhost')
mongoClient = MongoClient(f'mongodb://{MONGO}', 27017)
DB = mongoClient["my_db"]
COLLECTION = DB["my_collection"]

LOG = logging
LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)


class RedisResource:
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')

    EXTRACT_QUEUE_NAME = 'queue:extract'
    STATUS_QUEUE_NAME = 'queue:status'

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)


@app.route("/")
def hello():
    return "Hello World!"


@app.route("/api/upload_tgz", methods=['POST'])
def upload_tgz():
    file_name = request.args.get('filename')
    sid = request.args.get('sid')
    data = request.get_data()
    md5 = hashlib.md5(data).hexdigest()
    bucket_name = f"{md5}{int(time())}"

    try:
        # create bucket with md5 as bucket name
        minioClient.make_bucket(bucket_name)

    except BucketAlreadyOwnedByYou or BucketAlreadyExists:
        pass
    except ResponseError as err:
        print(err)
        return InternalServerError(err.message)

    # upload file to minio
    minioClient.put_object(bucket_name, file_name, io.BytesIO(data), len(data))

    json_packed = json.dumps({
        'sid': sid,
        'bucket_name': bucket_name,
        'file_name': file_name
    })

    print(json_packed)

    RedisResource.conn.rpush(RedisResource.EXTRACT_QUEUE_NAME, json_packed)

    status_json_packed = json.dumps({
        'sid': sid,
        'status': 'waiting'
    })

    RedisResource.conn.rpush(RedisResource.STATUS_QUEUE_NAME, status_json_packed)
    return Response(json_packed, status=HTTPStatus.OK)


@app.route('/send_status', methods=['POST'])
def send_status():
    socketio.emit('status-update', request.json, room=request.json.get('sid'))
    return Response(status=200)


@app.route('/api/download')
def download():
    if "bucket_name" and "file_name" not in request.args:
        return Response(status=400)
    bucket_name = request.args.get('bucket_name')
    file_name = request.args.get('file_name')

    db_bucket = COLLECTION.find_one({'bucket_name': bucket_name, 'file_name': file_name})
    status_completed = False if not db_bucket else db_bucket.get('status') == 'completed'

    if status_completed:
        resp = minioClient.get_object(bucket_name, file_name)
        headers = {'Content-Disposition': f'attachment; filename={file_name}'}
        return Response(response=resp, headers=headers)
    return Response(status=404)


if __name__ == '__main__':
    # app.run()
    socketio.run(app, host='0.0.0.0')
