#!/usr/bin/env python3
import logging
import json
import uuid
import os
import tarfile
import redis
from pymongo import MongoClient
from minio import Minio
from minio.error import (ResponseError, BucketAlreadyOwnedByYou,
                         BucketAlreadyExists)

LOG = logging
REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
QUEUE_NAME = 'queue:extract'

INSTANCE_NAME = uuid.uuid4().hex

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

DB_NAME = 'my_db'
COLLECTION_NAME = 'my_collection'

MINIO_URL = os.getenv('MINIO_URL', 'localhost')
MINIO_ACCESS_KEY = os.getenv('MINIO_ACCESS_KEY')
MINIO_SECRET_KEY = os.getenv('MINIO_SECRET_KEY')

minioClient = Minio(f'{MINIO_URL}:9000',
                    access_key=MINIO_ACCESS_KEY,
                    secret_key=MINIO_SECRET_KEY,
                    secure=False)

MONGO = os.getenv('MONGO', 'localhost')
mongoClient = MongoClient(f'mongodb://{MONGO}', 27017)


class RedisResource:
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
    CONVERT_QUEUE_NAME = 'queue:convert'
    STATUS_QUEUE_NAME = 'queue:status'

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)


def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    """
    Listen to Redis
    """
    active = True

    while active:
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            continue

        _, packed_task = packed

        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task)
            except TypeError:
                LOG.exception('json.loads failed')
            if task:
                callback_func(task)


def extract_tgz(log, task):
    sid = task.get('sid')
    bucket_name = task.get('bucket_name')
    tgz_name = task.get('file_name')

    status_json_packed = json.dumps({
        'sid': sid,
        'status': 'extracting'
    })

    RedisResource.conn.rpush(
        RedisResource.STATUS_QUEUE_NAME,
        status_json_packed
    )

    try:
        location = f'tmp/{bucket_name}/{tgz_name}'
        minioClient.fget_object(bucket_name, tgz_name, location)

        # my_db
        db = mongoClient[DB_NAME]

        # my_collection
        collection = db[COLLECTION_NAME]

        tf = tarfile.open(location)
        files = [f for f in tf.getnames() if '.pdf' in f and './._' not in f]

        log.info(files)
        total_size = len(files)
        data = {
            'bucket_name': bucket_name,
            'file_name': tgz_name,
            'cur_size': 0,
            'total_size': total_size,
            'files': [],
            'status': 'uncompleted'
        }

        collection.insert_one(data)

        tf.extractall(f'tmp/{bucket_name}')

        for file_name in files:
            minioClient.fput_object(bucket_name, file_name, f'tmp/{bucket_name}/{file_name}')
            RedisResource.conn.rpush(
                    RedisResource.CONVERT_QUEUE_NAME,
                    json.dumps({
                        'sid': sid,
                        'bucket_name': bucket_name,
                        'file_name': file_name,
                    }))

    except ResponseError as err:
        print(err)


def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn,
        QUEUE_NAME,
        lambda task_descr: extract_tgz(named_logging, task_descr))


if __name__ == '__main__':
    main()
