#!/usr/bin/env python3
""" Status worker """

import logging
import json
import uuid
import os
import redis
from minio import Minio
import requests

LOG = logging
REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
QUEUE_NAME = 'queue:status'

INSTANCE_NAME = uuid.uuid4().hex

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

MINIO_URL = os.getenv('MINIO_URL', 'localhost')
MINIO_ACCESS_KEY = os.getenv('MINIO_ACCESS_KEY')
MINIO_SECRET_KEY = os.getenv('MINIO_SECRET_KEY')

MINIO_CLIENT = Minio(f'{MINIO_URL}:9000',
                     access_key=MINIO_ACCESS_KEY,
                     secret_key=MINIO_SECRET_KEY,
                     secure=False)

WEB_CONTROLLER = os.getenv("WEB_CONTROLLER", "localhost")
SEND_STATUS_URL = f'http://{WEB_CONTROLLER}:5000/send_status'

class RedisResource:
    """ RedisResource is a class that is used to stored redis config """
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)


def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    """
    Listen to Redis
    """
    active = True

    while active:
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            continue

        _, packed_task = packed

        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task)
            except TypeError:
                LOG.exception('json.loads failed')
            if task:
                callback_func(task)


def execute_status_report(log, task):
    """
    Report status received from queue
    """
    requests.post(SEND_STATUS_URL, json=task)


def main():
    """ Main """
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn,
        QUEUE_NAME,
        lambda task_descr: execute_status_report(named_logging, task_descr))


if __name__ == '__main__':
    main()
